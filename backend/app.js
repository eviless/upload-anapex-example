const createError = require('http-errors');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const promisesAll = require('promises-all');
const base64 = require('base64-stream');
const {graphqlExpress, graphiqlExpress} = require('apollo-server-express');
const {apolloUploadExpress, GraphQLUpload} = require('apollo-upload-server');
const graphTools = require('graphql-tools');

const dataMock = require('./data');

const indexRouter = require('./routes/index');

const app = express();

const typeDefs = `
    scalar Upload
    type Query {
        collection (id: Int): App
    }
    type Mutation {
        collection (name: String!, files: [Upload!]!): App
    }
    type App {
        name: String
        filesSrc: [String]
    }
`;

const resolvers = {
    Upload: GraphQLUpload,
    Query: {
        collection: (root, args) => dataMock.filter((it, key) => key === args.id).pop()
    },
    Mutation: {
        collection: async (root, args) => {
            const filesSrc = (await promisesAll.all(args.files.map(processUpload))).resolve;

            return {name: args.name, filesSrc: filesSrc}
        }
    },
};

const storeFS = stream => {
    return new Promise((resolve, reject) => {
        fileBS64 = '';
        const bs64 = stream.pipe(base64.encode());
        bs64.on('data', (chunk) => {
            fileBS64 += chunk.toString();
        });
        bs64.on('end', () => {
            resolve('data:image/jpeg;base64,' + fileBS64);
        });
        bs64.on('error', () => {
            reject();
        });
    });
}

const processUpload = async upload => {
    const {stream, filename, mimetype, encoding} = await upload;
    return await storeFS(stream);
}

const schema = graphTools.makeExecutableSchema({
    typeDefs,
    resolvers,
})
app.use(cors());
app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/', indexRouter);
app.use('/graphql', apolloUploadExpress(), graphqlExpress({schema}));
app.use('/graphiql', graphiqlExpress({endpointURL: '/graphql'}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
