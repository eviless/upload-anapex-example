import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { App } from './app';
import { Collection } from "./Collection";

const getOne = gql`query collection ($id: Int) { collection(id: $id) { name, filesSrc }}`;
const update = gql`
mutation collection ($name: String!, $files: [Upload!]!) { 
  collection (name: $name, files: $files) { 
    name, 
    filesSrc 
  }
} 
`;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  model: App;
  submitted = false;

  constructor(public apollo: Apollo) {
  }

  ngOnInit(): void {
    this.model = new App('', null, []);
    this.apollo
      .query({query: getOne, variables: {id: 0}})
      .subscribe(item =>
        this.model = {
          name: (item.data as Collection).collection.name,
          files: null,
          filesSrc: (item.data as Collection).collection.filesSrc
        }
      );
  }

  onSubmit() {
    this.apollo.mutate({
      mutation: update,
      variables: {
        name: this.model.name,
        files: this.model.files
      }
    })
      .subscribe(item => {
          this.submitted = true;
          this.model = {
            name: (item.data as Collection).collection.name,
            files: null,
            filesSrc: (item.data as Collection).collection.filesSrc
          }
        }
      )
    ;
  }

  onFileChange(files) {
    this.model.files = files;
    this.model.filesSrc = [];
    Array.from(files).forEach(file => {
      var reader = new FileReader();
      reader.onload = (e) => {
        this.model.filesSrc.push((e.target as FileReader).result);
      }
      reader.readAsDataURL(file as File);
    });
  }
}
