export class App {
  constructor(
    public name: string,
    public files: FileList,
    public filesSrc: Array<String>
  ) {}
}
