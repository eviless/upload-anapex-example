Upload AnApEx example application
---------------------------------
It's a simple example for [apollo-upload-server](https://github.com/jaydenseric/apollo-upload-server/) and [apollo-upload-client](https://github.com/jaydenseric/apollo-upload-client) for [Angular](https://angular.io/) and [Express](http://expressjs.com)

Setup
-----
```docker compose up -d```

[anapex.frontend](http://anapex.frontend/) for frontend

[anapex.backend](http://anapex.backend/) for backend

[anapex.backend/graphiql](http://anapex.backend/graphiql/) for graphql console
